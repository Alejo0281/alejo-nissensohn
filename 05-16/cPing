#!/bin/bash

args=( $@ )
# $address tracks wether there is a valid IP address in the arguments
address=1

for opcion in "${!args[@]}"; do
	case ${args[$opcion]} in
	    -h|-H|--help)
		    echo "cPing -- Version 1.0"
		    echo "This script verifies ping arguments before they are input into said command"
		    echo "Usage ./cPing [-C n Number of pings attempted] [-p Protocol used] [-T Timestamp] [-b Broadcast]"
		    exit 0
		    ;;
	    -C) 
		cantidad=${args[$(($opcion+1))]}
		if [[ $cantidad -gt 0 ]]; then
			counter="-c $cantidad"
			continue
		fi
		echo "Error: '$cantidad' is not a valid -C value"
		exit 1
		;;
	    -T) 
		timestamp="-D"
		;;
	    -p) 
		proto=${args[$(($opcion+1))]}
		if [[ $proto != 4 && $proto != 6 ]];then
			echo "Error: '$proto' is not a valid -p value"
			exit 1
		fi
		p="-$proto"
		;;
	    -b) 
		b="-b"
		;;
	    *)
		def=${args[$opcion]}
		if [ "$def" = "${args[-1]}" ];then

			#Now that I know that $def is supposed to be the hostname, I check for it being IPv4 so I can verify the address as being real
			if [[ "$proto" -eq 4 ]];then
				oIFS="$IFS"
				IFS="."
				length=0
				for num in $def; do
					let "length++"
					for ((i=0; i<${#num}; i++)); do
						char=${num:$i:1}
						re='^[0-9]+$'
						if [[  char =~ $re ]];then
							echo "Error: IPV4 address '$def' contains characters not within Decimal parameters"
							exit 1
						fi
					done
					if [ $num -gt 255 ];then
						echo "Error: '$num' greater than 255, destination not valid, exiting"
						exit 1
					fi
				done
				if [ $length != 4 ];then
					echo "Error: IPv4 address '$def' has incorrect number of fields: $length"
					exit 1
				fi
				IFS="$oIFS"
				address=0
				continue

			#Then I check for IPv6
			elif [[ "$proto" -eq 6 ]];then
				oIFS="$IFS"
				IFS=":"
				length=0
				for num in $def; do
					let "length++"	
					if [ -z $num ];then
						continue
					fi
					for (( i=0; i<${#num}; i++ )); do
						char=${num:$i:1}
						((16#$char)) 2> /dev/null
						if [[ $? -ne 0 ]];then
							echo "Error: IPV6 address '$def' contains characters '$char' not within Hexadecimal parameters"
							exit 1
						fi
					done
				done
				
				if [ $length -gt 8 ];then
					echo "Error: IPv6 address '$def' has incorrect number of fields: $length"
					exit 1
				fi

				IFS="$oIFS"
				address=0
				continue
			fi
			#If it's neither an IPv4 nor IPv6 address, I check if the DNS can resolv the address, if it can't then the code exits with an error
			host $def &> /dev/null
			if [[ $? -eq 0 ]];then
				address=0
				continue
			fi
			echo "Error: Can't identify argument '$def' as a valid pingable host, exiting"
			exit 1
		fi

		case ${args[$(($opcion-1))]} in
			-C|-p)
				continue
				;;
		esac

		echo "Error: Unknown argument '$def'"
		exit 1
		;;
	esac
done

# check if there are enough arguments, requires to have at least 1 argument other then ping destination and ping destination is assumed to be there until next if
length=${#args[@]} 
if [ $length -lt 2 ];then
	echo "Error: Insuficient arguments"
	exit 1
fi

if [ $address -eq 1 ];then
	echo "Error: Missing destination, exiting"
	exit 1
fi

ping $b $timestamp $p $counter ${args[-1]}

#EOF
